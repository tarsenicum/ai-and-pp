#!/usr/bin/env swipl
% 1/2

% facts
cat(butsi).
cat(korni).
cat(mac).

dog(flash).
dog(rover).
dog(spot).

% cats
color(butsi, brown).
color(korni, black).
color(mac, red).
% endof cats
% dogs
color(flash, dot).
color(rover, red).
color(spot, white).
% endof dogs
% endof facts

% rules

pet(Object) :- cat(Object); dog(Object).

% ownership

owner(tom, Pet) :- pet(Pet), (color(Pet, black)); color(Pet, brown).
owner(kate, Pet) :- dog(Pet), not(owner(tom, Pet)), not(color(Pet, white)).
owner(alan, Pet) :- Pet=mac, not(owner(kate, butsi)), not(family(spot)).

% endof ownership

family(Pet) :- owner(tom, Pet); owner(kate, Pet).

% endof rules

% Queries

% pets without owner
no_owner(Pet) :- pet(Pet), not(owner(_, Pet)).

% all dog with colors
dogs_n_colors(Pet, Color) :- dog(Pet), color(Pet, Color).

% where Tom is owner
tom_is_owner(Pet) :- owner(tom, Pet).

% kates dogs
kates_dogs(Pet) :- dog(Pet), owner(kate, Pet).

% endof Queries