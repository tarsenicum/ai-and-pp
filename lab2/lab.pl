#!/usr/bin/env swipl
% 2/2

subSubList([], _). % terminator

subSubList([Hs|Ts], [Hl|Tl]) :-
    Hs = Hl -> % match first elements
    subSubList(Ts, Tl); % then match tails
    fail. % else not sublist

subList([], _). % terminator
subList([Hs|Ts], [Hl|Tl]) :-
    Hs = Hl -> % match first elements
    subSubList(Ts,Tl); % then match only tailes
    subList([Hs|Ts], Tl). % else match first list and tail of second list
